import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import ReactMarkdown from 'react-markdown'
import ListGroup from 'react-bootstrap/ListGroup';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';


function ReceptDetails(props){
    return (
        <div>
            <strong>Aantal personen</strong>: <span>{props.item.personen} </span> <br />
            <strong>Bereidingstijd</strong>: <span>{props.item.tijd} minuten</span>
            <hr />
            <h5>Ingredienten</h5>
            <div><ReactMarkdown>{props.item.ingredienten}</ReactMarkdown></div>
            <hr />
            <h5>Bereiding</h5>
            <div><ReactMarkdown>{props.item.bereiding}</ReactMarkdown></div>
        </div>
    )
}

function ReceptLink(props){
    const [showFull, setShowFull] = React.useState(false)
    const clickHandler = function(){  setShowFull(!showFull) }

    return (
        <div>
            <a onClick={clickHandler}><h4>{props.title}</h4></a>
            {showFull ? <ReceptDetails item={props.item} /> : null}
        </div>
    )
}


function ReceptList(props){
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  useEffect(() => {    // Update the document title using the browser API    document.title = `You clicked ${count} times`;  i
  fetch('http://localhost:5555')
    .then(response => response.json())
    .then(
        (result) => {
            setIsLoaded(true);
            setItems(result.recipes);
        },
        (error) => {
            setIsLoaded(true);
            setError(error);
        }  
    )
  },[]);

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
        <>
        <h1>{props.title}</h1>
        <hr />
        <ListGroup>
            {items.map(item => (
            <ListGroup.Item key={item.id}>
                <ReceptLink title={item.titel} created={item.created} item={item}></ReceptLink>
            </ListGroup.Item>
            ))}
        </ListGroup>
        </>
    );
  }
}

function App() {
  return (
    <Container>
        <Row>
            <Col>        
                <ReceptList title="Recepten" />
            </Col>
        </Row>
    </Container>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);


